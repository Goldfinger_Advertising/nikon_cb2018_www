$(document).ready(function() {
    // Hide mobile menu after scroll
    $(window).on("scroll", function() {
        var $menu = $("#mobile-menu");

        if ($menu.data("isVisible")) {
            $menu.data("isVisible", false);
            $menu.css({
                display: "none"
            });
        }
    });

    // Show mobile menu
    $("#mobile-menu-button").off("click").on("click", function() {
        var displayState = "none";
        var $menu = $("#mobile-menu");

        if ($menu.data("isInitialized") !== true) {
            $(".header > ul").clone().appendTo("#mobile-menu");
            $menu.data("isInitialized", true);
        }

        if ($menu.data("isVisible")) {
            displayState = "none";
            $menu.data("isVisible", false);
        }

        else {
            displayState = "block";
            $menu.data("isVisible", true);
        }

        $menu.height($(window).height());
        $menu.css({
            display: displayState
        });
    });
});